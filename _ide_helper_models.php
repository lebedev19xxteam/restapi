<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\FileStorage
 *
 * @method static Builder|Files newModelQuery()
 * @method static Builder|Files newQuery()
 * @method static Builder|Files query()
 * @mixin Eloquent
 * @property int $id
 * @property string $title
 * @property string $path
 * @property string $mimetype
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static Builder|Files whereCreatedAt($value)
 * @method static Builder|Files whereId($value)
 * @method static Builder|Files whereMimetype($value)
 * @method static Builder|Files wherePath($value)
 * @method static Builder|Files whereTitle($value)
 * @method static Builder|Files whereUpdatedAt($value)
 * @method static Builder|Files whereUserId($value)
 */
	class FileStorage extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\MainMenuItems
 *
 * @property int $id
 * @property string $name
 * @property string $link
 * @property int $image_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static Builder|MainMenuItems newModelQuery()
 * @method static Builder|MainMenuItems newQuery()
 * @method static Builder|MainMenuItems query()
 * @method static Builder|MainMenuItems whereCreatedAt($value)
 * @method static Builder|MainMenuItems whereId($value)
 * @method static Builder|MainMenuItems whereImageId($value)
 * @method static Builder|MainMenuItems whereLink($value)
 * @method static Builder|MainMenuItems whereName($value)
 * @method static Builder|MainMenuItems whereUpdatedAt($value)
 * @mixin Eloquent
 */
	class MainMenuItems extends \Eloquent {}
}

