FROM php:8.0-fpm-alpine3.13

# Install dev dependencies
RUN apk add --no-cache --virtual .build-deps \
    $PHPIZE_DEPS \
    curl-dev \
    libtool \
    libxml2-dev \
    postgresql-dev \
    libpng-dev

# Install production dependencies
RUN apk add --no-cache \
    bash \
    curl \
    g++ \
    gcc \
    git \
    libc-dev \
    libzip-dev \
    make \
    oniguruma-dev \
    postgresql-libs \
    rsync \
    zlib-dev \
    gettext \
    freetype-dev \
    libpng-dev \
    jpeg-dev \
    libmemcached-dev \
    libjpeg-turbo-dev

RUN docker-php-ext-install \
    bcmath \
    opcache\
    calendar \
    curl \
    gd \
    mbstring \
    pdo \
    pdo_mysql \
    pcntl \
    tokenizer \
    xml \
    zip \
    gd \
    sockets \
    fileinfo \
    exif

RUN apk add busybox-extras supervisor nginx

#install Imagick
RUN apk add --no-cache --virtual .phpize-deps $PHPIZE_DEPS imagemagick-dev libtool
RUN export CFLAGS="$PHP_CFLAGS" CPPFLAGS="$PHP_CPPFLAGS" LDFLAGS="$PHP_LDFLAGS"
RUN pecl install imagick
RUN docker-php-ext-enable imagick
RUN apk add --no-cache --virtual .imagick-runtime-deps imagemagick

#install Memcached
RUN pecl install memcached && docker-php-ext-enable memcached
RUN pecl install memcache && docker-php-ext-enable memcache

COPY docker-data/backend/php/php-fpm.d/*.conf /usr/local/etc/php-fpm.d/
COPY docker-data/backend/php/ini/*.ini /usr/local/etc/php/conf.d/
COPY docker-data/backend/nginx/*.conf /etc/nginx/
COPY docker-data/backend/nginx/conf.d /etc/nginx/conf.d
COPY docker-data/backend/nginx/nginx.conf.template /etc/nginx/nginx.conf.template

ADD docker-data/backend/supervisor/supervisor.ini /etc/supervisor.d/supervisor.ini
RUN mkdir /run/nginx

# Install composer
ENV COMPOSER_HOME /composer
ENV PATH ./vendor/bin:/composer/vendor/bin:$PATH
ENV COMPOSER_ALLOW_SUPERUSER 1
ENV PHP_IDE_CONFIG "serverName=ttndebug"
RUN curl -s https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin/ --filename=composer

# Setup working directory
WORKDIR /www/code

# create service directories
RUN mkdir -p /www/logs/nginx
RUN mkdir -p /www/var/tmp/images
RUN mkdir -p /www/log

COPY docker-data/backend/docker-entrypoint.sh /usr/local/bin/docker-app-entrypoint
RUN chmod +x /usr/local/bin/docker-app-entrypoint
ENTRYPOINT ["docker-app-entrypoint"]

COPY docker-data/backend/cron/crontab /etc/cron.d/ttn-cron
RUN chmod 0644 /etc/cron.d/ttn-cron
RUN crontab /etc/cron.d/ttn-cron
RUN touch /var/log/cron.log

ADD ./ /www/code
RUN composer install

RUN chown -R www-data:www-data /www

# Cleanup
RUN apk del -f .build-deps && rm -rf /tmp/* /var/cache/apk/*
RUN apk del .phpize-deps

ENV APP_ENV="${APP_ENV}"
ENV TELESCOPE_ENABLED="${TELESCOPE_ENABLED}"

ENV PHPFPM_MAX_CHILDREN=20
ENV PHPFPM_START_SERVERS=10
ENV PHPFPM_MIN_SPARE_SERVERS=5
ENV PHPFPM_MAX_SPARE_SERVERS=15
ENV NGINX_WORKER_CONNECTIONS=2048

ENV PHP_OPCACHE_VALIDATE_TIMESTAMPS=1

EXPOSE 80
CMD ["/www/code/start.sh"]
