<?php
namespace App\Services;


use App\Models\Files;
use Illuminate\Support\Facades\Storage;

class FileStorageService
{
    public function saveFile($name, $body)
    {
        $model = new Files();

        $explodeName = explode('.',$name);

        $model->title = $explodeName[0] ;
        $model->mimetype = $explodeName[1];
        $model->path = $this->buildDirectory($name, $body);
        $model->user_id = 1;

        $model->save();
        Storage::put(($model->path), $body);
    }

    public function getFile($id) : string
    {
        $model = Files::whereId($id)->firstOrFail();
        return $this->getFullUrl($model->path);
    }

    private function getFullUrl($path) : string
    {
        return $path;
    }

    private function buildDirectory($name, $body): string
    {
        $dir = implode('/', str_split(hash('md5', $body), 3));
        return $dir . '/' . $name;
    }
}
