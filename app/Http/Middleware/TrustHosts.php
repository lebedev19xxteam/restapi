<?php

namespace App\Http\Middleware;

use Illuminate\Http\Middleware\TrustHosts as Middleware;

class TrustHosts extends Middleware
{
    /**
     * Get the host patterns that should be trusted.
     *
     * @return array
     */
    public function hosts()
    {
        return [
//            $this->allSubdomainsOfApplicationUrl(),
            'localhost:3000',
            'localhost',
            'http://51.250.8.15:3000',
            'http://51.250.8.15',
        ];
    }
}
