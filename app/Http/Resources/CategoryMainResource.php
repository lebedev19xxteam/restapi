<?php


namespace App\Http\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

class CategoryMainResource extends JsonResource
{
    /**
     * @throws \Exception
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->link,
            'productCount' => random_int(1, 15),
            'image' => new ImageResource($this->image)
        ];
    }
}
