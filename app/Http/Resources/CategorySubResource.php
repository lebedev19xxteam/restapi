<?php


namespace App\Http\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

class CategorySubResource extends JsonResource
{

    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'link' => $this->link,
            'image' => new ImageResource($this->image)
        ];
    }
}
