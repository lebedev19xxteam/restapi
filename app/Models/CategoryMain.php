<?php


namespace App\Models;


use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\CategoryMain
 *
 * @property int $id
 * @property string $name
 * @property string $link
 * @property int $image_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static Builder|MainMenuItems newModelQuery()
 * @method static Builder|MainMenuItems newQuery()
 * @method static Builder|MainMenuItems query()
 * @method static Builder|MainMenuItems whereCreatedAt($value)
 * @method static Builder|MainMenuItems whereId($value)
 * @method static Builder|MainMenuItems whereImageId($value)
 * @method static Builder|MainMenuItems whereLink($value)
 * @method static Builder|MainMenuItems whereName($value)
 * @method static Builder|MainMenuItems whereUpdatedAt($value)
 * @mixin Eloquent
 * @property-read \App\Models\Files|null $image
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CategorySub[] $subMenu
 * @property-read int|null $sub_menu_count
 */
class CategoryMain extends Model
{
    protected $table = 'categories_main';

    protected $fillable = [
        'name',
        'link',
        'image_id',
    ];

    public function image()
    {
        return $this->hasOne(Files::class, 'id', 'image_id');
    }

    public function subMenu()
    {
        return $this->hasMany(CategorySub::class, 'main_menu_id', 'id');
    }
}
