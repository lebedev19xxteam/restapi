<?php


namespace App\Models;


use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Files
 *
 * @method static Builder|Files newModelQuery()
 * @method static Builder|Files newQuery()
 * @method static Builder|Files query()
 * @mixin Eloquent
 * @property int $id
 * @property string $title
 * @property string $path
 * @property string $mimetype
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static Builder|Files whereCreatedAt($value)
 * @method static Builder|Files whereId($value)
 * @method static Builder|Files whereMimetype($value)
 * @method static Builder|Files wherePath($value)
 * @method static Builder|Files whereTitle($value)
 * @method static Builder|Files whereUpdatedAt($value)
 * @method static Builder|Files whereUserId($value)
 */
class Files extends Model
{
    protected $table = 'files';

    protected $fillable = [
        'title',
        'path',
        'mimetype',
        'user_id'
    ];

    public function getFullPath()
    {
//        return 'http://localhost:8010' . $this->path;
        return env('FILESYSTEM_HOST') . $this->path;
    }
}
