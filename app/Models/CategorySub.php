<?php


namespace App\Models;


use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CategorySub
 *
 * @property int $id
 * @property int $main_menu_id
 * @property int|null $image_id
 * @property string $name
 * @property string $link
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\CategoryMain|null $category
 * @property-read \App\Models\Files|null $image
 * @method static Builder|CategorySub newModelQuery()
 * @method static Builder|CategorySub newQuery()
 * @method static Builder|CategorySub query()
 * @method static Builder|CategorySub whereCreatedAt($value)
 * @method static Builder|CategorySub whereId($value)
 * @method static Builder|CategorySub whereImageId($value)
 * @method static Builder|CategorySub whereLink($value)
 * @method static Builder|CategorySub whereMainMenuId($value)
 * @method static Builder|CategorySub whereName($value)
 * @method static Builder|CategorySub whereUpdatedAt($value)
 * @mixin Eloquent
 */
class CategorySub extends Model
{
    protected $table = 'categories_sub';

    protected $fillable = [
        'name',
        'link',
        'main_menu_id',
        'image_id'
    ];

    public function category()
    {
        return $this->hasOne(CategoryMain::class, 'id', 'main_menu_id');
    }

    public function image()
    {
        return $this->hasOne(Files::class, 'id', 'image_id');
    }
}
