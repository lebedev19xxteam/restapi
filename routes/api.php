<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\UserController;
use App\Http\Resources\CategoryMainResource;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\CategorySubWithCategoryResource;
use App\Http\Resources\ProductResource;
use App\Models\CategoryMain;
use App\Models\CategorySub;
use App\Models\Product;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// User group
Route::prefix('user')->group(function (){
    Route::middleware('auth:sanctum')->group(function (){
        Route::get('/me', [UserController::class, 'me']);
        Route::post('/update', [UserController::class, 'update']);
        Route::post('/logout', [AuthController::class, 'logout']);
        Route::post('/change_password', [UserController::class, 'changePassword']);
    });

    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/login', [AuthController::class, 'login']);
});



Route::get('/categories', function () {
    return CategoryMainResource::collection(CategoryMain::all());
});

Route::get('/category/{link}', function ($link) {
    return new CategoryResource(CategoryMain::whereLink($link)->first());
});

Route::get('/products/{category_id}', function ($category_id){
    return ProductResource::collection(Product::whereSubcategoryId($category_id)->get());
});

Route::get('/categories_sub', function () {
    return CategoryResource::collection(CategoryMain::all());
});
Route::get('/subcategory_with_category/{link}', function ($link) {
    return new CategorySubWithCategoryResource(CategorySub::whereLink($link)->first());
});

