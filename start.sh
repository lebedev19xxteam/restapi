#!/bin/sh
#if [[ "$APP_ENV" = "local" && "$TELESCOPE_ENABLED" = "true" ]]; then
#    php artisan telescope:publish
#fi
php artisan migrate
envsubst < /etc/nginx/nginx.conf.template > /etc/nginx/nginx.conf
supervisord --configuration /etc/supervisord.conf --nodaemon
