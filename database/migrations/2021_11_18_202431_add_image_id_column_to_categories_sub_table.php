<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddImageIdColumnToCategoriesSubTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories_sub', function (Blueprint $table) {
            $table->unsignedBigInteger('image_id')->nullable()->after('main_menu_id');
            $table->foreign('image_id', 'image_id_idx')->references('id')->on('files');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories_sub', function (Blueprint $table) {
            $table->dropForeign('image_id_idx');
            $table->dropColumn('image_id');
        });
    }
}
